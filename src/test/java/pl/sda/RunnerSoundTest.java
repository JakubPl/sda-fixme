package pl.sda;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class RunnerSoundTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void soutTest() {
        //given
        Runner runner = new Runner();
        //when
        runner.sounds();
        //then
        assertThat(outContent.toString()).isEqualTo("tu tu tu dum dum dum\r\n" +
                "dum dum dum dum dum dum dum dum\r\n" +
                "tu tu tu dum dum dum tu tu tu\r\ndum dum dum dum dum dum\r\n" +
                "owner name\r\n" +
                "street1\r\n" +
                "name\r\n" +
                "surname\r\n" +
                "M\r\n");
    }
}
