package pl.sda;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

public class FixThatTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;


    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void isEqual255() {
        //given
        Runner runner = new Runner();
        //when
        String input = "255";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        runner.playWithNumbers();
        //then

        assertThat(outContent.toString()).isEqualTo("Is equal 255\r\n");
    }

    @Test
    public void isNotEqual255() {
        //given
        Runner runner = new Runner();
        //when
        String input = "256";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        runner.playWithNumbers();
        //then

        assertThat(outContent.toString()).isEqualTo("Is not equal 255\r\n");
    }


    @Test
    public void isNotEqual9223372036854775808() {
        //given
        Runner runner = new Runner();
        //when
        String input = "9223372036854775809";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        runner.playWithBigNumbers();
        //then

        assertThat(outContent.toString()).isEqualTo("Is not equal 9223372036854775807\r\n");
    }

    @Test
    public void isEqual9223372036854775808() {
        //given
        Runner runner = new Runner();
        //when
        String input = "9223372036854775807";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        runner.playWithBigNumbers();
        //then

        assertThat(outContent.toString()).isEqualTo("Is equal 9223372036854775807\r\n");
    }
}
