package pl.sda;

import pl.sda.model.Address;
import pl.sda.model.Owner;
import pl.sda.model.OwnerBuilder;
import pl.sda.model.Sex;

public class Main {
    private static int howManyTimeThisMethodWasCalled = 0;



    /*

    "tu tu tu dum dum dum\r\n" +
                "dum dum dum dum dum dum dum dum\r\n" +
                "tu tu tu dum dum dum tu tu tu\r\ndum dum dum dum dum dum\r\n" +
                "owner name\r\n" +
                "street1\r\n" +
                "name\r\n" +
                "surname\r\n" +
                "M\r\n"

     */
    public static void main(String[] args) {
        Runner runner = new Runner();
        runner.sounds();
        //runner.playWithNumbers();

        //new Address("321", "Katowice", "40-007", "123");

        Address katowice = Address.builder()
                .houseNumber("123")
                .street("321")
                .town("Katowice")
                .zipCode("40-007")
                .build();


        Owner owner = new OwnerBuilder()
                .address(katowice)
                .name("Jakub")
                .surname("P")
                .sex(Sex.MALE)
                .createOwner();

        //runner.playWithBigNumbers();
    }
}
