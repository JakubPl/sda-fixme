package pl.sda;

//import pl.sda.model.Drum;

import pl.sda.model.Address;
import pl.sda.model.Owner;
import pl.sda.model.OwnerBuilder;
import pl.sda.model.Sex;
import pl.sda.model.instruments.*;

import java.math.BigInteger;
import java.util.Scanner;

public class Runner {
    public void sounds() {
        Drum drum = new Drum();
        Sax sax = new Sax();

        InstrumentComposite instrumentComposite = new InstrumentComposite();
        instrumentComposite.add(sax);
        instrumentComposite.add(drum);
        instrumentComposite.makeSound();

        //new DrumAndDrumAndDrum().makeSound();
        new InstrumentComposite(new Drum(), new Drum(), new Drum()).makeSound();
        //new DrumAndGuitar().makeSound();
        new InstrumentComposite(new Drum(), new Guitar()).makeSound();
        //new SaxAndDrumAndSax().makeSound();
        new InstrumentComposite(new Sax(), new Drum(), new Sax()).makeSound();


//new Address("street1", "town", "zipCode", "134", null, "owner name")
        Address address = Address.builder()
                .houseNumber("134")
                .town("town")
                .street("street1")
                .zipCode("zipCode").build();
        Owner owner = new OwnerBuilder()
                .sex(Sex.MALE)
                .name("name")
                .surname("surname")
                .address(address)
                .createOwner();

        Owner owner1 = new OwnerBuilder()
                .sex(Sex.MALE)
                .name("owner 1")
                .surname("surname")
                .address(address)
                .createOwner();

        Owner owner2 = new OwnerBuilder()
                .sex(Sex.MALE)
                .name("owner 2")
                .surname("surname")
                .address(address)
                .createOwner();

        //why it doesnt work!
        final Drum drum1 = new Drum();

        drum.setOwner(owner1);

        final Drum drum2 = new Drum();

        drum.setOwner(owner2);

//        System.out.println(drum1.getOwner().getName() + " and " + drum2.getOwner().getName());


/*        Instrument instrument = new InstrumentComposite(new Drum(), new Drum());
        instrument.setOwner("owner 1 and owner 2");
        instrument.makeSound();

        System.out.println(owner.getAddress().getOwnerName());
        System.out.println(owner.getStreet());
        System.out.println(owner.getName());
        System.out.println(owner.getSurname());
        System.out.println(owner.getSex());*/

    }

    public void playWithNumbers() {
        Scanner scanner = new Scanner(System.in);
        final Integer someInt = Integer.valueOf(scanner.nextLine());
        final Integer numberToCompare = 255;

        if (someInt.equals(numberToCompare)) {
            System.out.println("Is equal 255");
        } else {
            System.out.println("Is not equal 255");
        }
    }

    public void playWithBigNumbers() {
        //tego nie ruszamy
        Scanner scanner = new Scanner(System.in);
        BigInteger longFromScanner = new BigInteger(scanner.nextLine());
        BigInteger numberToCompare = BigInteger.valueOf(9_223_372_036_854_775_807L);
        numberToCompare = numberToCompare.add(BigInteger.ONE);//9223372036854775808
        longFromScanner = longFromScanner.add(BigInteger.ONE); //9223372036854775807 + 1 = 9223372036854775807
        //az do tego miejsca
        //wymienic operatory
        // && || ! ^ | == & + - / * < > <= >=
        // ? :




        if (numberToCompare.compareTo(longFromScanner) == 0) {
            System.out.println("Is equal 9223372036854775807");
        } else {
            System.out.println("Is not equal 9223372036854775807");
        }
    }

    public String test() {
        boolean arg=true;
        /*if(arg) {
            return "a";
        } else {
            return "b";
        }*/
        return arg ? "a" : "b";
    }


}
