package pl.sda.model;

public class Owner {
    private String name;
    private String surname;
    private Address address;
    private Sex sex;

    public Owner(String name, String surname, Address address, Sex sex) {
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
