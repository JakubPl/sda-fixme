package pl.sda.model;

public enum Sex {
    MALE("M"),
    FEMALE("F");

    Sex(String readable) {
        //System.out.println("Called! - " + readable);
        this.readableName = readable;
    }
    private String readableName;

    public String getReadableName() {
        return readableName;
    }
}

class Test {
    public static void main(String[] args) {
        //1. Sex.MALE == Sex.FEMALE;
        //2. Sex.MALE.equals(Sex.FEMALE);
        //3. Obojetne

        Sex a = Sex.MALE;
        Sex b = Sex.MALE;


        System.out.println(Sex.MALE.getReadableName());
        System.out.println(Sex.FEMALE.getReadableName());
    }
}


