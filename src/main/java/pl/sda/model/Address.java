package pl.sda.model;

public class Address {
    private final String street;
    private final String town;
    private final String zipCode;
    private final String houseNumber;

    private Address(String street, String town, String zipCode, String houseNumber) {
        this.street = street;
        this.town = town;
        this.zipCode = zipCode;
        this.houseNumber = houseNumber;
    }

    private Address(AddressBuilder addressBuilder) {
        this.street = addressBuilder.street;
        this.town = addressBuilder.town;
        this.zipCode = addressBuilder.zipCode;
        this.houseNumber = addressBuilder.houseNumber;
    }

    public String getStreet() {
        return street;
    }

    public String getTown() {
        return town;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public static AddressBuilder builder() {
        return new AddressBuilder();
    }

    public static class AddressBuilder {
        private String street;
        private String town;
        private String zipCode;
        private String houseNumber;

        public AddressBuilder street(String street) {
            this.street = street;
            return this;
        }

        public AddressBuilder town(String town) {
            this.town = town;
            return this;
        }

        public AddressBuilder zipCode(String zipCode) {
            this.street = zipCode;
            return this;
        }

        public AddressBuilder houseNumber(String houseNumber) {
            this.houseNumber = houseNumber;
            return this;
        }

      /*  public Address build() {
            return new Address(street, town, zipCode, houseNumber);
        }*/

        public Address build() {
            return new Address(this);
        }
    }
}
