package pl.sda.model.instruments;

import java.util.LinkedList;
import java.util.List;


//TODO: wprowadzic setOwner, getOwner, setModel, getModel
public class InstrumentComposite extends Instrument {
    List<Instrument> instrumentList = new LinkedList<>();

    public InstrumentComposite(Instrument... instruments) {
        if (instruments != null) {
            for (Instrument instrument : instruments) {
                instrumentList.add(instrument);
            }
        }
    }



    public void add(Instrument instrument) {
        instrumentList.add(instrument);
    }

    @Override
    public void makeSound() {
        instrumentList.forEach(instrument -> instrument.makeSound());
    }
}
