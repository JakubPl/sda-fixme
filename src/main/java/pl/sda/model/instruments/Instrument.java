package pl.sda.model.instruments;

import pl.sda.model.Owner;

public abstract class Instrument {
    private String model;
    private Owner owner;
    public abstract void makeSound();

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
}
