package pl.sda.model;

public class OwnerBuilder {
    private String name;
    private String surname;
    private Address address;
    private Sex sex;

    public OwnerBuilder name(String name) {
        this.name = name;
        return this;
    }

    public OwnerBuilder surname(String surname) {
        this.surname = surname;
        return this;
    }

    public OwnerBuilder address(Address address) {
        this.address = address;
        return this;
    }

    public OwnerBuilder sex(Sex sex) {
        this.sex = sex;
        return this;
    }

    public Owner createOwner() {
        return new Owner(name, surname, address, sex);
    }
}